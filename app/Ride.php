<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ride
 *
 * Rides table model.
 *
 * @package App
 */
class Ride extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'date', 'source_id', 'destination_id'
    ];

    /**
     * Maps rides to user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
  	{
    	return $this->belongsTo('App\User');
  	}

    /**
     * Maps source place to ride.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
  	public function source()
    {
        return $this->belongsTo('App\Place');
    }

    /**
     * Maps destination place to ride.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function destination()
    {
        return $this->belongsTo('App\Place');
    }

    public $timestamps = false;
}
