<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>EasyCab</title>
        <style>
			#nojsdiv {
				border: 1px solid red;
				padding: 10px
			}
			#nojsspan {
				color:red;
			}
			#closejs {display:none;}
	    </style>
	</head>
	<body>
<div class="modal fade" id="newride">
	<div class="modal-dialog">
		<div class="modal-content">
			{{ Form::open(['method' => 'POST', 'url' => 'home', 'class' => 'form-horizontal', 'id' => 'new-ride']) }}
			 {{ csrf_field() }}
			<div class="modal-header">
				<button id="closejs" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<noscript>
                    <a href="{{ url('/home') }}">&times;</a>
                </noscript>
				<h4 class="modal-title">Book new ride</h4>
			</div>
			<div class="modal-body">
				<label>Pick up address</label>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">Street</div>
						{!! Form::text('street', Input::old('street'), ['class' => 'form-control', 'placeholder' => 'Street',
							'id' => 'street']) !!}
						{{ csrf_field() }}
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">Building</div>
						{!! Form::text('building', Input::old('building'), ['class' => 'form-control', 'placeholder' => 'Building',
							'id' => 'building']) !!}
						{{ csrf_field() }}
					</div>
				</div>
				<label>Destination address</label>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">Street</div>
						{!! Form::text('destStreet', Input::old('destStreet'), ['class' => 'form-control', 'placeholder' => 'Street',
							'id' => 'destStreet']) !!}
						{{ csrf_field() }}
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">Building</div>
						{!! Form::text('destBuilding', Input::old('destBuilding'), ['class' => 'form-control', 'placeholder' => 'Building',
							'id' => 'destBuilding']) !!}
						{{ csrf_field() }}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::submit('Book', ['class'=>'btn', 'id'=>'sbmt']) !!}
				{{ csrf_field() }}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<noscript>
		<div id="nojsdiv">
        	<span id="nojsspan">Sorry, JavaScript is not enabled!</span>
      	</div>
</noscript>
</body>
</html>
