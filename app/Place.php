<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Place
 *
 * Places table model.
 *
 * @package App
 */
class Place extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street', 'building'
    ];

    /**
     * Maps places to rides.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ride()
  	{
    	return $this->hasMany('app\Ride');
  	}
  	public $timestamps = false;
}
