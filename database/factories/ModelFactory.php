<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('qwerty'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Place::class, function (Faker\Generator $faker) {
    return [
        'city' => $faker->city,
        'street' => $faker->streetName,
        'building' => $faker->randomDigit
    ];
});

$factory->define(App\Ride::class, function (Faker\Generator $faker) {
    return [
        'price' => $faker->randomNumber(3),
        'start_time' => $faker->dateTime,
        'end_time' => $faker->dateTime,
        'source_id' =>factory(App\Place::class)->create()->id,
        'destination_id' =>factory(App\Place::class)->create()->id,
        'user_id' =>factory(App\User::class)->create()->id
    ];
});
