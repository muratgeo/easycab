@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New ride</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('/home') }}">
                        {{ csrf_field() }}

                        <div class="panel">Source place</div>
                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                            <label for="street" class="col-md-4 control-label">Street</label>

                            <div class="col-md-6">
                                <input id="street" class="form-control" name="street" value="{{ old('street') }}" required autofocus>

                                @if ($errors->has('street'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('building') ? ' has-error' : '' }}">
                            <label for="building" class="col-md-4 control-label">Building</label>

                            <div class="col-md-6">
                                <input id="building" class="form-control" name="building" value="{{ old('building') }}" required autofocus>

                                @if ($errors->has('building'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('building') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="panel">Destination place</div>
                        <div class="form-group{{ $errors->has('destStreet') ? ' has-error' : '' }}">
                            <label for="destStreet" class="col-md-4 control-label">Street</label>

                            <div class="col-md-6">
                                <input id="destStreet" class="form-control" name="destStreet" value="{{ old('destStreet') }}" required autofocus>

                                @if ($errors->has('destStreet'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destStreet') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('destBuilding') ? ' has-error' : '' }}">
                            <label for="destBuilding" class="col-md-4 control-label">Building</label>

                            <div class="col-md-6">
                                <input id="destBuilding" class="form-control" name="destBuilding" 
                                    value="{{ old('destBuilding') }}" required autofocus>

                                @if ($errors->has('destBuilding'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destBuilding') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Book
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<noscript>
        <div id="nojsdiv">
            <span id="nojsspan">Sorry, JavaScript is not enabled!</span>
        </div>
</noscript>
@endsection
