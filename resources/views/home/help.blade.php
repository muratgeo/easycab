<div class="modal fade" id="help">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Help</h4>
			</div>
			<div class="modal-body">
				<h4>
					EasyCab helps you to travel easier than ever.
				</h4>
				<p>
					Click "New ride" button to book new ride and fill the form and that's it.
				</p>
				<p>
					You can see your rides on your home page.
				</p>
			<div class="modal-footer">
			</div>
			</div>
		</div>
	</div>
</div>