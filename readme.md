<p align="center"><img src="http://www.taxi-stmoritz.com/wp-content/uploads/2016/07/taxi1.jpg"></p>

## About Easycab

Laravel is a web taxi application.

[Try it here](http://whispering-falls-21196.herokuapp.com)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## Local execution

If you want to use this app on your local computer,
you should run:

    git clone https://gitlab.fel.cvut.cz/muratgeo/easycab

    cd ourtube

    composer install

    php artisan migrate

    php artisan serve

also you will need to change your database settings in your .env file.
