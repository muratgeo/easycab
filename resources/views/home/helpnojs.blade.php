<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>EasyCab</title>
	</head>
	<body>
		<h4>
			EasyCab helps you to travel easier than ever.
		</h4>
		<p>
			Click "New ride" button to book new ride and fill the form and that's it.
		</p>
		<p>
			You can see your rides on your home page.
		</p>

		<style>
			#nojsdiv {
				border: 1px solid purple;
				padding: 10px
			}
			#nojsspan {
				color:red;
			}
		</style>
		<div id="nojsdiv">
		    <span id="nojsspan">Sorry, JavaScript is not enabled!</span>
		</div>
	</body>
</html>