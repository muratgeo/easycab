$(document).ready(function() {
    $('#newride').on('submit', function(e){
        e.preventDefault();
        var building = $('#building');
        var street = $('#street');
        var destBuilding = $('#destBuilding');
        var destStreet = $('#destStreet');

        var data = {
            'street' : street.val(),
            'building' : building.val(),
            'destBuilding' : destBuilding.val(),
            'destStreet' : destStreet.val()
        }
        var valid = validate(street) && validate(building) && validate(destStreet) && validate(destBuilding);
        if(valid) {
            setWarning(street, true);
            setWarning(building, true);
            setWarning(destStreet, true);
            setWarning(destBuilding, true);
            $.ajax({
                url: 'home',
                type: 'post',
                data: data,
                dataType: 'json',
                success: function(data){
                    $('#newride').modal('hide');
                    //location.reload(); 
                    $( '#dash' ).load(location.href + " #dash");
                }
            });   
          
        } else {
            setWarning(street, validate(street));
            setWarning(building, validate(building));
            setWarning(destStreet, validate(destStreet));
            setWarning(destBuilding, validate(destBuilding));
        }  
    });
});

function validate(inputobject) {
    switch(inputobject[0].id) {
        case "destStreet":
        case "street": return (inputobject.val().length > 2 && inputobject.val().length < 45);
            break;
        case "destBuilding":
        case "building": return ($.isNumeric(inputobject.val()) && parseInt(inputobject.val()) > 0);
            break;
        default:
            break;
    }  
}

function setWarning(inputobject, valid) {
    if (valid) inputobject.closest('.input-group').removeClass('has-error').addClass('has-success');
    else inputobject.closest('.input-group').removeClass('has-success').addClass('has-error');
}