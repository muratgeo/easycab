<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Place;
use App\Ride;
use Input;


/**
 * Class NewRideController
 *
 * Handle creating new ride.
 *
 * @package App\Http\Controllers
 */
class NewRideController extends Controller
{
    /**
     * Validate users input
     * and create new ride if input is correct.
     * If ride is between places
     * that have already been visited
     * price doesn't change.
     * Redirect to app dashboard
     * @param Request $request
     * @return Redirect
     */
	protected function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'street' => 'required|min:2|max:45',
            'building' => 'required|digits_between:1,4',
            'destStreet' => 'required|min:2|max:45',
            'destBuilding' => 'required|digits_between:1,4'
        ]);

        if ($validator->fails()) {
            error_log($validator->errors());
            $input = Input::all();
            $input['autoOpenModal'] = 'true';   //Add the auto open indicator flag as an input.
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput($input);     
        } else {

            $data = $request->all();
            $place = Place::firstOrCreate([
                'street' => $data['street'],
                'building' => $data['building']
            ]);

            $destPlace = Place::firstOrCreate([
                'street' => $data['destStreet'],
                'building' => $data['destBuilding']
            ]);

            $price = random_int(50, 500);
            $ride = Ride::where('source_id', $place->id)->where('destination_id', $destPlace->id)->get();

            if (sizeof($ride) > 0)  $price = $ride[0]->price;

            $ride = Ride::where('source_id', $destPlace->id)->where('destination_id', $place->id)->get();

            if (sizeof($ride) > 0)  $price = $ride[0]->price;

            Auth::user()->rides()->create([
                'price' => $price,
                'date' => $today = Carbon::today()->toDateString(),
                'source_id' => $place->id,
                'destination_id' => $destPlace->id,
                'user_id' => Auth::user()->id
            ]);

            //return redirect('/home');
            return response()->json(['success' => 'success']);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    protected function index() {
        $today = Carbon::now()->formatlocalized('%a %d %b %Y');
        return view('home.home', compact('today'));
    }

    /**
     * Returns view with new ride form for no js browsers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function nojsmodal() {
        return view('home.nojsmodal');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
}