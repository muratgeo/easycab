Specification
==============

Web app in PHP, where users can book taxi rides.

Each user have a dashboard with all rides listed.

The app must be secure against XSS, CSRF, SQL injection.