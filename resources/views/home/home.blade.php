@extends('layouts.app')

@section('content')
<div id="dash" class="container">
    <div class="row text-left">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>My rides <span class="pull-right">{{ $today }}</span></h2>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="pull-right">
                    <a id="new-ride-btn" class="btn btn-primary" data-toggle="modal" href='#newride'>New ride</a>
                    <noscript>
                        <style>
                            #new-ride-btn {display:none;}
                        </style>
                        <a class="btn btn-primary" data-toggle="modal" href="{{ url('/new-ride') }}">New ride</a>
                    </noscript>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if (Auth::user()->rides->count() > 0)
                    <div class="table-responsive">
                        <table class="table table-hover table-condenced">
                            <thead>
                                <tr class="text-capitalize">
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            @foreach (Auth::user()->rides as $ride)
                                <tbody>
                                    <tr>
                                        <td>{{ $ride->source->street }} {{$ride->source->building}}</td>
                                        <td>{{ $ride->destination->street }} {{$ride->destination->building}}</td>
                                        <td>{{ $ride->price }}</td>
                                    </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                @else
                    <p>You have no rides yet :(</p>
                @endif
            </div>
        </div>
        @include('home.modal')
    </div>
    <noscript>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">
                Logout
            </button>
        </form>
        <a class="btn btn-primary" href="{{ url('/help') }}">Help</a>
    </noscript>
</div>

@endsection
